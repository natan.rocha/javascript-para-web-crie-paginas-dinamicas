const tocaSom = (seletorAudio) => {
    const elemento = document.querySelector(seletorAudio);
    if(elemento && elemento.localName === 'audio'){
        elemento.play();   
    } else{
        alert('Elemento não encontrado')
    }
};

const listaDeTeclas = document.querySelectorAll('.tecla');


for (let i = 0; i < listaDeTeclas.length; i++) {
    const tecla = listaDeTeclas[i];
    const instrumento = tecla.classList[1];
    const idAudio = `#som_${instrumento}`

    listaDeTeclas[i].onclick = () =>{
        tocaSom(idAudio);
        
    };

    tecla.onkeydown = (event) =>{
        if(event.code == 'Space' || event.code == 'Enter'){
            tecla.classList.add('ativa')

        }
    }
    tecla.onkeyup = () =>{
        tecla.classList.remove('ativa')
    }
}
